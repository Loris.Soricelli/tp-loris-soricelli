//Calcul de moyenne
//Affichage de la moyenne
//Loris Soricelli
//Le 16/03/2020
                    

let listeNotes = [];                                                //Liste pour stocké les valeurs entrées
var moyenne = 0;                                                    //Moyenne des nombres

while (promptText !== "f") {                                        //Boucle du prompt, sort de la boucle si l'utilisateur rentre f
    var promptText = prompt("Choisissez le nombre de départ");      //Demande a l'utilisateur de la valeur
    listeNotes.push(promptText);                                    //On étend d'une valeur la liste 
}
listeNotes.pop()                                                    //Suppresion de la derniere valeur (ici f)
for (var i = 0; i < listeNotes.length; i++) {                       //boucle permettant de parcourir le tableau
    moyenne += Number(listeNotes[i]);                               //Conversion par en "number" des valeurs du tableau
}
moyenne = moyenne / i;                                              //Calcul de la moyenne

//affichage de la liste des nombres entées et de la moyenne
alert("pour les notes \" " + listeNotes + " \" la moyenne est de: " + moyenne);
