//Loris Soricelli
//Le 24/03/2020

$(document).ready(function() {

    let etat = 0;                                           //Fonction permetant de savoir "ou" le programe en est (texte ouvert ou fermé)
    $("#maDiv1").hide();                                    //Execute dans un premier temps "hide" cachant le texte
    $("#btn1").attr("value", "Ouvrir");                     //Affiche dans le bouton dans un premier temps ouvrir
    $("#maDiv1").html("Mon texte affiché grâce à jQuery");  //Si click alors il affiche le texte
    $("#btn1").click(Div);                                  //Initialisation de la fonction Div si on click sur le bouton (#btn1)

    function Div() {
        //Si l'etat est a 0 il affichera le texte et changera le bouton en "fermé"
        if (etat === 0) {
            $("#maDiv1").show();
            $("#btn1").attr("value", "Fermer");
            etat = 1
        //Si l'etat est a 1 il cachera le texte et changera le bouton en "ouvrir"
        } else {
            $("#maDiv1").hide();
            $("#btn1").attr("value", "Ouvrir");
            etat = 0
        }
    }
});