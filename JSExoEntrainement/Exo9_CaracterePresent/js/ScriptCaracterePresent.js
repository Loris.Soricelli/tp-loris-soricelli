//Affiche si le texte saisi par l'utilisateur contient la lettre 'e'
//Loris Soricelli
//Le 19/03/2020

let texte = prompt("Entrez un texte");          //Initialisation de la variable texte par la demande a l'utilisateur du texte a saisir
let nombre = texte.length;                      //On initialise nombre par le nombre de caractére présent dans la variable texte
let present;                                    //La variable présent vas contenir la lettre présente lors du calcul (elle changera donc a chaque tour de la boucle)
let nbe = 0;                                    //La variable "e" va compté le nombre de "e" présent (Pour savoir si le texte en contient aumoin 1)

//Boucle permettant de sortir chaque caractére du texte a la suite
for (let i = 0; i !== nombre; i++) {
    present = texte.charAt(i);
//boucle ajoutant le nombre de "e" à chaque passage a l'intérieur
    if (present == "e") {
        nbe++;
    }
}
//boucle: si le texte ne contient pas de "e" il l'affiche sinon il affiche qu'il y a un ou des "e"
if (nbe == 0) {
    alert("Le texte rentré ne contient pas de \"e\"");              //affichage si il n'y a pas de "e" dans le texte
} else {
    alert("Le texte rentré contient un ou des \"e\"");              //affichage si il y a un ou des "e" dans le texte
}