//Conversion euro - dollar
//Affiche les conversions euros vers dollars
//Mise en place d'une fonction modifiant le nombre de conversion affiché
//Loris Soricelli
//Le 17/03/2020

affConvEuroDollar(100)                                      //Modification du parametre de la fonction : Nombre a atteindre en dollar       


function affConvEuroDollar(prmValMaxEuro) {                 //Fonction avec un parametre

    const TAUX = 1.65;                                      //Taux de change 1€ = 1.65$
    let valEuro = 1;                                        //Valeur de l'euro
    let valDollar = valEuro * TAUX;                         //Multiplication de la valeur de l'euro par le taux de change
    let affFinal = "";

    //Boucle calculant la conversion jusqu'a atteindre la valeur demandée
    while (valEuro <= prmValMaxEuro) {
        affFinal = affFinal + valEuro + "euro(s) = " + valDollar.toFixed(2) + "dollar(s)" + "\n";   //Concaténation de la valeur en € + la valeur en $ (2 chiffres aprés la virgule)
        valEuro = valEuro * 2;                                                                      //On multiplie par 2 pour passé a la prochaine conversion
        valDollar = valEuro * TAUX;                                                                 //Conversion en multipliant par le taux
    }
    alert(affFinal);                                                                                //Affichage du résultat de la conversion
}
