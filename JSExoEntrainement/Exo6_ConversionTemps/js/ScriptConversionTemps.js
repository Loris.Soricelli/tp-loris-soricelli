//Conversion d'un nombre entier de secondes saisi au départ en un nombre d’années, de mois, de jours, de minutes et de secondes 
//Loris Soricelli
//Le 18/03/2020
ConversionTemps(5000)

function ConversionTemps(prmSecondes) {
    var annees = Math.floor(prmSecondes / 31536000);
    var mois = Math.floor((prmSecondes % 31536000 )/ 2628000);
    var jours = Math.floor(((prmSecondes % 31536000 ) % 2628000) / 86400);
    var heures = Math.floor((((prmSecondes % 31536000 ) %2628000) % 86400) / 3600);
    var minutes = Math.floor(((((prmSecondes % 31536000 ) %2628000) % 86400) % 3600) / 60);
    var secondes = (((prmSecondes %2628000) % 86400) % 3600) % 60;
    alert(annees + " ans " + mois + " mois " + jours + " jours " + heures + " heures " + minutes + " minutes " + secondes + " secondes");
}

