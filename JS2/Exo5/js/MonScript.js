//Loris Soricelli
//Le 25/03/2020
$(document).ready(function () {
  $("#lien").click(function (prmEvenement) {
    //desactive la fonctionnement basique du lien
    prmEvenement.preventDefault();
    //remplacement la balise <a> par une balise <img>
    $("#lien").replaceWith("<img src='images/tpinfo.jpg' alt=''/>");
  });
});
