//Affichage sur une ligne toutes les lettres de l'alphabet en majuscule
//Loris Soricelli
//Le 19/03/2020



    let resultat = "";                            //Le résultat affiché en chaine de caratére

    //Boucle de l'affichage des lettres de l'alphabet (Initialisé à 65 pour le caractére A en Unicode)
    for (let i = 65; i !== 91; i++) {
    resultat += String.fromCharCode(i) + " ";
    }
    document.write(resultat);                              //Affichage du résultat
