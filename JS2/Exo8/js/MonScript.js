//Loris Soricelli
//Le 30/03/2020
$(document).ready(function () {
  //Calcul de la somme des valeurs du tableau
  function calculTotal() {
    var total = 0;
    $("tbody tr td").each(function () {
      total += parseInt($(this).html());
    });
    $("tfoot tr td").html(total);
  }

  //Ajout d'une valeur
  $("#btnajouter").click(function () {
    $("tbody").append("<tr><td></td></tr>");
    $("tbody tr td").last().html($("tbody tr td").length);
    calculTotal();
  });

  //Supression de la derniere valeur du tableau
  $("#btnsupr").click(function () {
    $("tbody tr:last-child").remove();
    calculTotal();
  });
});
