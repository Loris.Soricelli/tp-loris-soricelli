//Calcul du volume d'un parallélépipède rectangle
//Loris Soricelli
//Le 18/03/2020

let longueur = prompt("Longueur :");                //Demande a l'utilisateur de la longueur
let largeur = prompt("Largeur :");                  //Demande a l'utilisateur de la largeur
let hauteur = prompt("Hauteur :");                  //Demande a l'utilisateur de la hauteur

let resultat = "Le volume du parallélépipède rectangle est de : " + longueur * largeur * hauteur + " cm3";      //Initialisation du resultat par le resultat du calcul

alert(resultat);                                    //Affichage du résultat