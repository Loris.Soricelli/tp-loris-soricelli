//Affichage une suite de 12 nombres dont chaque terme est égal au triple du terme précédent. 
//Mise en place d'une fonction modifiant le nombre triplé pour reusabilité du code
//Loris Soricelli
//Le 17/03/2020

modificationNombreTriple(10)                                             //Appel de la fonction modifiant la valeur triplé

function modificationNombreTriple(prmNombre) {                          //Fonction avec le paramètre

    let resultat = "Nombre de départ : " + prmNombre + " \n";           //On initialise le resultat par l'affichage du nombre entré dans le paramètre + le nombre entré                     
    const triple = 3;


    //Boucle des 12 premiers termes de la table

    for (let i = 0; i !== 12; i++) {
        prmNombre = prmNombre * triple;               //Calcul : On triple le nombre par la constante
        resultat += prmNombre + " \n";                //Concaténation dans résultat de ce qu'il sera affiché dans l'alerte + retour a la ligne
    }
    alert(resultat);                                  //Affichage du résultat
}