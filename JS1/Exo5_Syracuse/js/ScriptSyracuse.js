//La conjecture de Syracuse
//Affichage de la conjecture de Syracuse, du temps de vol et de la valeur maximale
//Loris Soricelli
//Le 16/03/2020

let nb = prompt("Choisissez le nombre de départ");      //Nombre entré par l'utilisateur
let affFinal = "";                                      //Affichage final chaine de caractères 
let tpsDeVol = 0;                                       //Compteur d'itération
let alt = 0;                                            //Valeur maximale de la suite 


nombre = nb;
//Boucle de calcul, temps que le résultat n'est pas = 1 on reste dans la boucle
while (nombre !== 1) {
    if (nombre % 2 === 0) {         //Si le nombre est pair (=0)
        nombre = nombre / 2;        //On le divise par deux
        tpsDeVol++;                 //Incrémentation du compteur 
    }
    else {                          //Si le nombre est impair (=1) 
        nombre = nombre * 3 + 1;    //Divise par 3 puis ajoute 1
        tpsDeVol++;                 //Incrémentation du compteur 
    }
    if (nombre > alt) {             //si nombre est superieur à alt
        alt = nombre;
    }
    affFinal += nombre + " - ";     //Concaténation des nombres obtenus pour l'affichage
}
affFinal = affFinal.substring(0, affFinal.length - 2);  //On retire les 2 derniers caractère pour l'affichage (le tiret et l'espace)
//Affichage du nombre de départ nb, de syracuse, du temps de vol et de l'altitude maximale atteinte
alert("Suite de Syracuse pour " + nb + " = \n" + nb + " - "+ affFinal + "\n" + "Temps de vol = " + tpsDeVol + "\n" + "Altitude maximale = " + alt);