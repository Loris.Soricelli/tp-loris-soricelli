//Affichage des 20 premiers termes de la table de multiplication par 7.
//Mise en place d'une fonction modifiant le nombre multiplié pour reusabilité du code
//Loris Soricelli
//Le 17/03/2020

modificationMultiplicateur(7)

function modificationMultiplicateur(prmMultiplicateur) {  

let resultat = "";                                      //Le resultat en chaine de charactére

//Boucle des 20 premiers termes de la table
for (let i = 0; i !== 20; i++) {
    resultat += prmMultiplicateur + " x " + i + " = " + prmMultiplicateur * i + "\n";      //Concaténation dans résultat de ce qu'il sera affiché dans l'alerte + calcul + retour a la ligne
}
alert(resultat)                                         //Affichage du résultat

}