//Affichage une suite de 12 nombres dont chaque terme est égal au triple du terme précédent.
//Mise en place d'une fonction modifiant le nombre de termes de la suite affiché
//Loris Soricelli
//Le 17/03/2020
fibonacci(17)                                           //Appel de la fonction modifiant le nombre de termes

function fibonacci(prmFibonacci){                       //Fonction avec le paramètre

var a = 0;                                              //la variable par la 1ere valeur de la suite de Fibonacci
var b = 1;                                              //la variable par la 2eme valeur de la suite de Fibonacci
var resultat = b;                                       //On initialise resultat qui est la variable contenant le calcul de la valeur 1 et 2 par b
let affichage = "";                                     //L'affichage du résulat

//Boucle des 17 premiers termes de Fibonacci
for (let i = 0; i < prmFibonacci; i++) {
    affichage += resultat + "\n";                       //concaténation du résultat dans l'affichage + retour a la ligne
    resultat = a + b;                                   //Calcul de a+b
    a = b;                                              //On remplace l'ancienne valeur de a par b
    b = resultat;                                       //On remplace b par le resultat du calcul de a+b
}
alert(affichage)                                        //Affichage du résultat
}