//Affichage des 20 premiers termes de la table de multiplication par 7 et de si le résultat est un multiple de 3
//Mise en place d'une fonction modifiant le nombre multiplié pour reusabilité du code
//Loris Soricelli
//Le 19/03/2020

modificationMultiplicateur(7)

function modificationMultiplicateur(prmMultiplicateur) {

    let resultat = "";                            //Le résultat affiché en chaine de caratére
    let res;                                      //Le resultat du calcul

    //Boucle des 20 premiers termes de la table
    for (let i = 0; i !== 20; i++) {
        res = prmMultiplicateur * i;              //Calcul de la multiplication

    //Boucle déterminant si le resultat est un multiple de 3
        if ((res % 3 === 0) && (res !== 0)) {
            resultat += prmMultiplicateur + " x " + i + " = " + res + "*" + "\n";  //Concaténation dans résultat de ce qu'il sera affiché dans l'alerte si c'est un multiple : calcul + * + retour a la ligne
        } else {
            resultat += prmMultiplicateur + " x " + i + " = " + res + "\n";      //Concaténation dans résultat de ce qu'il sera affiché dans l'alerte si ce n'est pas un multiple : calcul + retour a la ligne
        }

    }
    alert(resultat);                              //Affichage du résultat
}