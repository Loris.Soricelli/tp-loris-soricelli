//Affiche si le texte saisi par l'utilisateur contient la lettre 'e' ainsi que le nombre de e. 
//Loris Soricelli
//Le 19/03/2020

let texte = prompt("Entrez un texte");          //Initialisation de la variable texte par la demande a l'utilisateur du texte a saisir
let nombre = texte.length;                      //On initialise nombre par le nombre de caractére présent dans la variable texte
let present;                                    //La variable présent vas contenir la lettre présente lors du calcul (elle changera donc a chaque tour de la boucle)
let nbe = 0;                                    //La variable nbe va compté le nombre de "e" présent ou si il y a aumoin un "e" 

//Boucle permettant de sortir chaque caractère du texte a la suite
for (let i = 0; i !== nombre; i++) {
    present = texte.charAt(i);
//boucle ajoutant le nombre de "e" à chaque passage a l'intérieur
    if (present == "e") {
        nbe++;
    }
}
//boucle: si il n'y a pas de "e" il l'affiche, sinon il affiche qu'il y en a et combien
if (nbe == 0) {
    alert("Le texte rentré ne contient pas de \"e\"");              //affichage si il n'y a pas de e si nbe est égal a 0
} else {
    alert("Le texte rentré contient " + nbe + " \"e\"");            //affichage du nombre de e si nbe est supérieur a 0
}